//
//  main.c
//  TwoFloats
//
//  Created by Geroen Joris on 8/12/13.
//  Copyright (c) 2013 Big Nerd Ranch. All rights reserved.
//

#include <stdio.h>

int main(int argc, const char * argv[])
{

    // Float 1
    float float1 = 3.14;
    
    // Float 2
    float float2 = 42.0;
    
    // Sum
    double sum = float1 + float2;
    
    // Log to user
    printf("The sum of these numbers is %f.\n", sum);
    
    
    return 0;
}

