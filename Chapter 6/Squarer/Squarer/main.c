//
//  main.c
//  Squarer
//
//  Created by Geroen Joris on 8/12/13.
//  Copyright (c) 2013 Big Nerd Ranch. All rights reserved.
//

#include <stdio.h>

int computeSquare(int number) {
    return number * number;
}

int main(int argc, const char * argv[])
{
    int number = 5;
    printf("\"%d\" squared is \"%d\"\n", number, computeSquare(number));
    return 0;
}

