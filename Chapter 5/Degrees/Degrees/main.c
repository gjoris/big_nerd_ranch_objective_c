//
//  main.c
//  Degrees
//
//  Created by Geroen Joris on 8/12/13.
//  Copyright (c) 2013 Big Nerd Ranch. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>

// Declare a static variable
static float lastTemperature = 50.0;

float fahrenheitFromCelcius(float cel) {
    lastTemperature = cel;
    float fahr = cel * 1.8 + 32.0;
    printf("%f Celcius is %f Fahrenheit\n", cel, fahr);
    return fahr;
}

int main(int argc, const char * argv[])
{
    float freezeInC = 0;
    float freezeInF = fahrenheitFromCelcius(freezeInC);
    printf("Water freezes at %f degrees Fahrenheit.\n", freezeInF);
    printf("The last temperature converted was %f.\n", lastTemperature);
    return EXIT_SUCCESS;
}

